# test_line

## example1
### conditions
```  
#simulation setting
  timestep = 1000 #10cycle
  dt = 0.1
  na = 10
  sim_obj = sim.simulation(timestep,dt,na)


  #road setting
  length = 10000
  shape = 'line'
  road = rd.road(length,shape)
  
  #initial parameter
  initial_length = 100
  initial_rho = 20/100.0
  

  #agent setting
  ps = numpy.linspace(0,initial_length,na)


  #boundary condition
  amplitude = 5 #speed amplitude
  mean = 8

  cycle = 10
  theta = numpy.linspace(0,2*cycle*numpy.pi,timestep)
  
  velocity = amplitude * numpy.sin(theta) + mean

  #initial condition
  vl = 16
  max_v 33.33
  reaction_t = 1
  agent = []

  for i in range(na-1):
    md = krauss.krauss()
    st = krauss.state(position = ps[i],velocity = 16)
    pp = krauss.property(dt=dt,max_v = 33.33,reaction_t=1)
    agent.append(ag.agent(md,st,pp))

  md = given.given()
  st = given.state(position = ps[na-1],velocity = velocity[0])
  pp = given.property(dt=dt)
  agent.append(ag.agent(md,st,pp))

```
![result](./figure_react1.svg)  
```![result](./number1_react1.png)```    
```![result](./number2_react1.png)```    
![result](./react1.mp4)


##simulation setting2
```
  #simulation setting
  timestep = 1000 #10cycle
  dt = 0.1
  na = 10
  sim_obj = sim.simulation(timestep,dt,na)


  #road setting
  length = 10000
  shape = 'line

  road = rd.road(length,shape)
  
  #initial parameter
  initial_length = 100
  initial_rho = 20/100.0
  

  #agent setting
  ps = numpy.linspace(0,initial_length,na)


  #boundary condition
  amplitude = 5 #speed amplitude
  mean = 8

  cycle = 10
  theta = numpy.linspace(0,2*cycle*numpy.pi,timestep)
  
  velocity = amplitude * numpy.sin(theta) + mean


  #initial condition
  vl = 16
  mxv = 33.33
  react = 10
  agent = []
  for i in range(na-1):
    md = krauss.krauss()
    st = krauss.state(position = ps[i],velocity = vl)
    pp = krauss.property(dt=dt,max_v = mxv,reaction_t=react)
    agent.append(ag.agent(md,st,pp))

  md = given.given()
  st = given.state(position = ps[na-1],velocity = velocity[0])
  pp = given.property(dt=dt)
  agent.append(ag.agent(md,st,pp))
 
```
![result](./figure_react10.svg)  
```![result](./number1_react10.png) ```   
```![result](./number2_react10.png) ```   

##simulation setting3
circle のバージョン
```
def main():
  #simulation setting
  timestep = 1000
  dt = 0.1
  na = 20
  sim_obj = sim.simulation(timestep,dt,na)
  


  #road setting
  length = 1000
  shape = 'circle'
  road = rd.road(length,shape)


  #agent setting
  ps = numpy.linspace(0,length-1,na)

  for i in range(na):
    ps[i] = ps[i] +  numpy.random.rand()

  agent = []

  for i in range(na):
    md = krauss.krauss()
    st = krauss.state(position = ps[i],velocity = 16)
    pp = krauss.property(dt=dt,max_v = 33.33)
    agent.append(ag.agent(md,st,pp))

  road.d_update(agent)

  sim_obj.set_agent(agent)
  sim_obj.set_road(road)
 
```

![result](./out.mp4)





## 密度ごとの伝搬テスト
n=10  
l=100  
![result](./figure_10_1001.png)  

n = 100  
l = 100  
![result](./figure_100_1001.png)  



