
import simulation.simulation as sim
import agent.agent as ag
import road.road as rd
from model import krauss
import numpy
import time
import matplotlib.pyplot as plt



def line2circle(result,length):
  theta = 2*numpy.pi*result/length 
  cos = numpy.cos(theta)
  sin = numpy.sin(theta)
  return cos,sin
 

def main():
  #simulation setting
  timestep = 1000
  dt = 0.1
  na = 3
  sim_obj = sim.simulation(timestep,dt,na)
  


  #road setting
  length = 1200
  shape = 'line'
  road = rd.road(length,shape)


  #agent setting
  ps = numpy.linspace(0,200,na)

  for i in range(na):
    ps[i] = ps[i] +  numpy.random.rand()

  agent = []

  for i in range(na):
    md = krauss.krauss()
    st = krauss.state(position = ps[i],velocity = 16)
    pp = krauss.property(dt=dt,max_v = 33.33)
    agent.append(ag.agent(md,st,pp))

  road.d_update(agent)

  sim_obj.set_agent(agent)
  sim_obj.set_road(road)
  

  t_start = time.time() 
  

  result =[]
  for i in range(timestep):
    sim_obj.agent_state_update()
    temp = numpy.zeros(na)
    for j in range(na):
      temp[j]=sim_obj.agent[j].state.p
      print(sim_obj.agent[j].state.v)
    result.append(temp)

  for i in range(timestep):
    x,y=line2circle(result[i],length)
    plt.scatter(x,y,s=5)
    plt.xlim(-1.1,1.1)
    plt.ylim(-1.1,1.1)
    plt.savefig(str(i).zfill(4)+".png")
    plt.close()

    
  t_end = time.time() 

  print(t_end-t_start)



  
main() 





