from abc import ABCMeta,abstractmethod

class Model(metaclass=ABCMeta):

  @abstractmethod
  def v_update(self):
    pass

