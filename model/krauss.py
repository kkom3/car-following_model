from model import model as m

class krauss(m.Model):
  def __init__(self):
    pass

  def v_update(self,state,proper):
     
    self.safe(state,proper)
    self.accel(state,proper)

    choice = {'max_v':proper.max_v,'accel':self.v_accel,'safe':self.v_safe}
    state.v_state = min(choice,key=choice.get)

    temp = state.v 
    state.v = choice[state.v_state]
    state.accel_now = (state.v - temp) / proper.dt


  
  def safe(self,state,proper):
    if state.first:
      state.d = 9999999999
    self.v_safe = state.v_lead + (state.d - state.v_lead * proper.reaction_t)/((state.v_lead + state.v)/(2*proper.max_deac) + proper.reaction_t)
    
   
  def accel(self,state,proper):
    self.v_accel = state.v + proper.accel*proper.dt


class state:
  def __init__(self,position=0,velocity=1,distance=1,v_lead=1,v_state=None,first=False, step=0, accel_now = 0,v_before = 0):
    self.p = position
    self.v = velocity
    self.v_before = v_before
    self.d = distance
    self.v_lead =  v_lead
    self.v_state = v_state
    self.first = first
    self.step = step
    self.accel_now = accel_now

class property:
  def __init__(self,reaction_t=5,max_v=27,max_deac=5,accel=3,dt=0.001,length=5,v_id=None ):
    self.reaction_t = reaction_t
    self.max_v = max_v
    self.max_deac = max_deac
    self.accel = accel
    self.dt = dt
    self.length = length
    self.v_id = v_id
