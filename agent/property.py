class property:
  def __init__(self,reaction_t=5,max_v=17,max_deac=5,accel=3,dt=0.001):
    self.reaction_t = reaction_t
    self.max_v = max_v
    self.max_deac = max_deac
    self.accel = accel
    self.dt = dt
