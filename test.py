
import simulation.simulation as sim
import agent.agent as ag
import road.road as rd
from model import krauss
import numpy
import time
import matplotlib.pyplot as plt
import road.equipment as eq





def line2circle(result,length):
  theta = 2*numpy.pi*result/length 
  cos = numpy.cos(theta)
  sin = numpy.sin(theta)
  return cos,sin
 

def main():
  #simulation setting
  timestep = 1000
  dt = 0.1
  na = 3
  sim_obj = sim.simulation(timestep,dt,na)
  


  #road setting
  length = 1200
  shape = 'line'
  road = rd.road(length,shape)


  #agent setting
  ps = numpy.linspace(0,200,na)


  agent = []

  for i in range(na):
    md = krauss.krauss()
    if i == 0:
      st = krauss.state(position = ps[i],velocity = 0,first=True)
    st = krauss.state(position = ps[i],velocity = 0,first=False)
    pp = krauss.property(dt=dt,max_v = 33.33)
    agent.append(ag.agent(md,st,pp))







  

  road.d_update(agent)

  sim_obj.set_agent(agent)
  sim_obj.set_road(road)

  sim_obj.agent_state_update()
  

  



  
main() 





