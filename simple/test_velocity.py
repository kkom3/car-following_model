
import simulation.simulation as sim
import agent.agent as ag
import road.road as rd
from model import simple
import numpy
import time
import matplotlib.pyplot as plt



def line2circle(result,length):
  theta = 2*numpy.pi*result/length 
  cos = numpy.cos(theta)
  sin = numpy.sin(theta)
  return cos,sin
 

def main():

  mean_v = []
  for num in range(90,110):
    #simulation setting
    timestep = 1000
    dt = 0.05
    na = num
    sim_obj = sim.simulation(timestep,dt,na)
    


    #road setting
    length = 1000
    shape = 'circle'
    road = rd.road(length,shape)


    #agent setting
    ps = numpy.linspace(0,length-length/na,na)

    for i in range(na):
      ps[i] = ps[i] + numpy.random.rand()

    agent = []

    for i in range(na):
      md = simple.simple()
      st = simple.state(position = ps[i],velocity = 16)
      pp = simple.property(dt=dt,max_v = 33.33)
      agent.append(ag.agent(md,st,pp))

    road.d_update(agent)

    sim_obj.set_agent(agent)
    sim_obj.set_road(road)
    

    t_start = time.time() 
    

    result =[]
    for i in range(timestep):
      sim_obj.agent_state_update()
      temp = numpy.zeros(na)
      for j in range(na):
        temp[j]=sim_obj.agent[j].state.v
      result.append(temp)

    print(na,' ',numpy.average(result[timestep-1]))

    mean_v.append([na,numpy.average(result[timestep-1])])


    
  

    t_end = time.time() 

  with open('mean_v.txt','wt') as f:
    for i in range(len(mean_v)):
      f.write(str(mean_v[i][0]) +' '+ str(mean_v[i][1]) + '\n')

  mean_v = numpy.array(mean_v).T

  plt.plot(mean_v[0],mean_v[1],marker='o')
  plt.savefig('mean_v.png')
  plt.close()
main() 





