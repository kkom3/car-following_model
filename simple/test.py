
import simulation.simulation as sim
import agent.agent as ag
import road.road as rd
from model import simple
import numpy
import time
import matplotlib.pyplot as plt



def line2circle(result,length):
  theta = 2*numpy.pi*result/length 
  cos = numpy.cos(theta)
  sin = numpy.sin(theta)
  return cos,sin
 

def main():
  #simulation setting
  timestep = 1000
  dt = 0.05
  na = 92
  sim_obj = sim.simulation(timestep,dt,na)
  


  #road setting
  length = 1000
  shape = 'circle'
  road = rd.road(length,shape)


  #agent setting
  ps = numpy.linspace(0,length-length/na,na)

  for i in range(na):
    ps[i] = ps[i] + numpy.random.rand()

  agent = []

  for i in range(na):
    md = simple.simple()
    st = simple.state(position = ps[i],velocity = 16)
    pp = simple.property(dt=dt,max_v = 33.33)
    agent.append(ag.agent(md,st,pp))

  road.d_update(agent)

  sim_obj.set_agent(agent)
  sim_obj.set_road(road)
  

  t_start = time.time() 
  

  result =[]
  for i in range(timestep):
    sim_obj.agent_state_update()
    temp = numpy.zeros(na)
    for j in range(na):
      temp[j]=sim_obj.agent[j].state.p
#      print(sim_obj.agent[j].state.v)
#      print(sim_obj.agent[j].state.d)
#      print(sim_obj.agent[j].state.v_state)
    result.append(temp)

  for i in range(timestep):
    x,y=line2circle(result[i],length)
    g = plt.subplot()
    g.scatter(x,y,s=5)
    plt.xlim(-1.1,1.1)
    plt.ylim(-1.1,1.1)
    g.set_aspect('equal')
    plt.savefig(str(i).zfill(4)+".png")
    plt.close()

    
  t_end = time.time() 

  print(t_end-t_start)



  
main() 





