
import simulation.simulation as sim
import agent.agent as ag
import road.road as rd
from model import simple
from model import given
import numpy
import time
import matplotlib.pyplot as plt



def main():
  #simulation setting
  timestep = 1000 #10cycle
  dt = 0.1
  na = 10
  sim_obj = sim.simulation(timestep,dt,na)


  #road setting
  length = 10000
  shape = 'line'
  road = rd.road(length,shape)
  
  #initial parameter
  initial_length = 100
  initial_rho = 20/100.0
  

  #agent setting
  ps = numpy.linspace(0,initial_length,na)


  #boundary condition
  amplitude = 5 #speed amplitude
  mean = 8

  cycle = 10
  theta = numpy.linspace(0,2*cycle*numpy.pi,timestep)
  
  velocity = amplitude * numpy.sin(theta) + mean


  #initial condition
  vl = 16
  mxv = 33.33
  agent = []
  for i in range(na-1):
    md = simple.simple()
    st = simple.state(position = ps[i],velocity = vl)
    pp = simple.property(dt=dt,max_v = mxv)
    agent.append(ag.agent(md,st,pp))

  md = given.given()
  st = given.state(position = ps[na-1],velocity = velocity[0])
  pp = given.property(dt=dt)
  agent.append(ag.agent(md,st,pp))

  sim_obj.set_road(road)
  sim_obj.set_agent(agent)


  #initial running
  result = []
  initial_timestep = 1000
  for i in range(1,1000):
    sim_obj.agent_state_update()
    temp = numpy.zeros((na,3))
    #for j in range(na):
      #temp[j][0]=sim_obj.agent[j].state.d
      #temp[j][1]=sim_obj.agent[j].state.v
      #temp[j][2]=sim_obj.agent[j].state.p

    #result.append(temp)
    #print('position',sim_obj.agent[na-2].state.p,' ',sim_obj.agent[na-1].state.p)
    #print('velocity',sim_obj.agent[na-2].state.v,' ',sim_obj.agent[na-1].state.v)


  #simulation execution

  for i in range(1,timestep):
    sim_obj.agent_state_update()
    temp = numpy.zeros((na,3))
    for j in range(na):
      temp[j][1]=sim_obj.agent[j].state.v
      temp[j][2]=sim_obj.agent[j].state.p

    result.append(temp)
    sim_obj.agent[na-1].state.v = velocity[i]
    #print(sim_obj.agent[na-1].state.p)
  

  data = []
  for i in range(na):
    data.append([])

  for i in range(na):
    for j in range(timestep-1):
      data[i].append(result[j][i][1])
   
  x = numpy.linspace(0,timestep,timestep-1)
  fig = plt.figure()
  for i in range(na):
    ax = fig.add_subplot(1,1,1)
    ax.plot(x,numpy.array(data[i]))
  plt.savefig('figure_react1.svg')

  fig = plt.figure(figsize=(7,2))
  ax = fig.add_subplot(1,1,1)
  ax.plot(x,numpy.array(data[na-1]))
  plt.savefig('number1_react1.png')

  fig = plt.figure(figsize=(7,2))
  ax = fig.add_subplot(1,1,1)
  ax.plot(x,numpy.array(data[na-2]))
  plt.savefig('number2_react1.png')




  with open('line_result.txt',mode='w') as f:
    for i in range(len(result)):
      f.write(str(result[i])+'\n')

    


main()




