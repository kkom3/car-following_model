# test_line

## CIRCLE
### conditions

```
  #simulation setting
  timestep = 1000
  dt = 0.05
  na = 25
  sim_obj = sim.simulation(timestep,dt,na)
  


  #road setting
  length = 1000
  shape = 'circle'
  road = rd.road(length,shape)


  #agent setting
  ps = numpy.linspace(0,length-length/na,na)

  for i in range(na):
    ps[i] = ps[i] + numpy.random.rand()

  agent = []

  for i in range(na):
    md = simple.simple()
    st = simple.state(position = ps[i],velocity = 16)
    pp = simple.property(dt=dt,max_v = 33.33)
    agent.append(ag.agent(md,st,pp))

  road.d_update(agent)

  sim_obj.set_agent(agent)
  sim_obj.set_road(road)
``` 

![result40](./40.mp4)   
![result20](./20.mp4)  
![result10](./10.mp4)  



- 車両台数vs平均車速  
![result90-110](./mean_v.png)  
![result92](./92.mp4)  

## LINE 
### conditions 
```
  #simulation setting
  timestep = 1000 #10cycle
  dt = 0.1
  na = 10
  sim_obj = sim.simulation(timestep,dt,na)


  #road setting
  length = 10000
  shape = 'line'
  road = rd.road(length,shape)
  
  #initial parameter
  initial_length = 100
  initial_rho = 20/100.0
  

  #agent setting
  ps = numpy.linspace(0,initial_length,na)


  #boundary condition
  amplitude = 5 #speed amplitude
  mean = 8

  cycle = 10
  theta = numpy.linspace(0,2*cycle*numpy.pi,timestep)
  
  velocity = amplitude * numpy.sin(theta) + mean


  #initial condition
  vl = 16
  mxv = 33.33
  agent = []
  for i in range(na-1):
    md = simple.simple()
    st = simple.state(position = ps[i],velocity = vl)
    pp = simple.property(dt=dt,max_v = mxv)
    agent.append(ag.agent(md,st,pp))

  md = given.given()
  st = given.state(position = ps[na-1],velocity = velocity[0])
  pp = given.property(dt=dt)
  agent.append(ag.agent(md,st,pp))

  sim_obj.set_road(road)
  sim_obj.set_agent(agent)


  #initial running
  result = []
  initial_timestep = 1000
  for i in range(1,1000):
    sim_obj.agent_state_update()
    temp = numpy.zeros((na,3))
``` 

- 速度変化の伝搬  
![number1_react1](./number1_react1.png)   
![number2_react1](./number2_react1.png)  

![react1_movie](./react1.mp4)   



