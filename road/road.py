import numpy

class road:
  def __init__(self,length=1000,element=[],shape=None):
    self.length = length
    self.shape = shape
    self.element =  element



  def p_update(self,agent):

    if self.shape=='circle':
      for i in range(len(agent)):
        p2 = agent[i].state.p + agent[i].state.v * agent[i].proper.dt

        if p2 > self.length:
          p2 = p2 - self.length
        agent[i].state.p = p2

    elif self.shape=='line':
      for i in range(len(agent)):
        p2 = agent[i].state.p + agent[i].state.v * agent[i].proper.dt
        agent[i].state.p = p2


    return agent
    

  def d_update(self,agent):

    ##----------------------------
    if self.shape=='circle':
      p = numpy.array([agent[i].state.p for i in range(len(agent))])
      l = numpy.array([agent[i].proper.length for i in range(len(agent))])
      mx = numpy.argmax(p)
      end = len(agent) - 1
      for i in range(len(agent)):
        follow = i
        lead = i+1
        if(i==end):
          lead = 0
				#distance = p[lead] - p[follow]
        #print('lead-follow-pos'+str(p[lead])+','+str(p[follow]))
        distance = p[lead] - p[follow] - l[lead]/2 - l[follow]/2 #distance is based on position and length of leader and follower. 
        if distance < 0:
          raise RoadClassError("Collision Detect : Distance is negative")
        if i==mx:
          distance = self.length - p[follow] + p[lead]
        agent[i].state.d = distance
        

 
    ##----------------------------
    if self.shape == 'line':
      p = numpy.array([agent[i].state.p for i in range(len(agent))])
      l = numpy.array([agent[i].proper.length for i in range(len(agent))])
      for i in range(len(agent)-1):
        follow = i
        lead = i+1
				#distance = p[lead] - p[follow]
        #print('lead-follow-pos'+str(p[lead])+','+str(p[follow]))
        #print(p)
        distance = p[lead] - p[follow] - l[lead]/2 - l[follow]/2 #distance is based on position and length of leader and follower. 
        print('distance')
        print(distance)
        if distance < 0:
          raise RoadClassError("Collision Detect : Distance is negative")
        agent[i].state.d = distance

    return(agent)
  
  def set_object(self,agent):
    for i in range(self,object):
      if object[i]=='tls':
        pass




  def v_lead_update(self,agent):

    ##----------------------------
    if self.shape=='circle':
      end = len(agent) - 1
      for i in range(len(agent)):
        follow = i
        lead = i+1
        if(i==end):
          lead = 0
        agent[follow].state.v_lead = agent[lead].state.v

    ##----------------------------
    if self.shape=='line':
      for i in range(len(agent)-1):
        follow = i
        lead = i+1
        agent[follow].state.v_lead = agent[lead].state.v

    return(agent)


class RoadClassError(Exception):
	pass



        


      


  

 
