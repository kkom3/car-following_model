import numpy
import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def distance(x,length):

  d = numpy.zeros(len(x))
  mx = numpy.argmax(x)
  end = len(x) - 1

  for i in range(len(x)):
    follow = i
    lead = i+1
    if(i==end):
      lead = 0

    distance = x[lead] - x[follow]

    if(i==mx):
      distance = x[lead] + float(length) - x[follow]


    d[i] = distance

  return d


def v_safe(vl,vf,d,reaction_t,mx_deac):
  return vl + (d-vl*reaction_t)/((vl+vf)/(2*mx_deac) + reaction_t)

def v_choice(max_v,v,a,v_sf,dt):
  print(max_v)
  if min(max_v,v+a*dt,v_sf) == max_v:
    print("max_v")
  elif min(max_v,v+a*dt,v_sf) == v+a*dt:
    print("v+a*dt")
  else:
    print("v_sf")
  return min(max_v,v+a*dt,v_sf)

def v_update(d,v,a,reaction_t,mx_deac,max_v,dt):
  v2 = numpy.zeros(len(d))
  for i in range(len(d)):
    follow = i
    lead = i+1
    if(i==len(d)-1):
      lead = 0
    vl = v[lead] 
    vf = v[follow]
    df = d[follow]
    v_sf = v_safe(vl,vf,df,reaction_t,mx_deac)
    v2[i] = v_choice(max_v,v[i],a,v_sf,dt)
  return v2

def x_update(v,x,dt,length):
  x2 = numpy.zeros(len(v))
  x2 = x + v * dt
  
  for i in range(len(x2)):
    if x2[i] > length:
      x2[i] = x2[i]-length
  return x2
  
def line2circle(x,length):
  theta = 2*numpy.pi*x/length 
  
  cos = numpy.cos(theta)
  sin = numpy.sin(theta)
  return cos,sin
  


      

    
    






def main():
  parser = argparse.ArgumentParser(description = "hogehoge")
  parser.add_argument('-n','--num',type=int,help='car num')
  parser.add_argument('-l','--length',type=int,help='length')
  parser.add_argument('-t','--timestep',type=int,help='number of timestep')
  options = parser.parse_args()

  initial_v = 0
  reaction_t = 1
  mx_deac =  5
  dt = 0.1
  max_v = 33.33
  a = 3

  

  x = numpy.linspace(0,options.length-options.length/float(options.num),options.num) + numpy.random.rand()
  v = numpy.zeros(options.num) + 10*initial_v
  for i in range(len(v)):
    v[i] = v[i] +  numpy.random.rand()
  y = numpy.zeros(len(x))
  
  ims =  []
  xy = []
  for i in range(options.timestep):
    print(i)
    fig = plt.figure(dpi=256)
    d = distance(x,options.length)
    v2 = v_update(d,v,a,reaction_t,mx_deac,max_v,dt)
    x2 = x_update(v,x,dt,options.length)

    v = v2
    print(numpy.mean(v))
    print(numpy.mean(d))
    print(numpy.var(d))
    print(d)
    x = x2
    cos,sin = line2circle(x,options.length)
    plt.scatter(cos,sin,s=5)
    plt.xlim(-1.1,1.1)
    plt.ylim(-1.1,1.1)
    plt.savefig(str(i).zfill(4)+".png")
    plt.close()
    v[0] = 0
  









main()  














