import numpy
from abc import ABCMeta,abstractmethod


class equipment:
  def __init__(self,state,proper,model):
    self.model = model
    self.state = state
    self.proper = proper

  def state_update(self):
    self.model.state_update(self.state,self.proper)


class equipment_meta(metaclass=ABCMeta):

  @abstractmethod
  def state_update(self):
    pass


class tls(equipment_meta):
  def __init__(self):
    pass

  def state_update(self,state,proper):
    mod_sum_val = state.step % proper.cycle
    if 0 =< mod_sum_val and mod_sum_val < pattern[0]:
      state.color = 0
    elif pattern[0] =< mod_sum_val and mod_sum_val < pattern[1]:
      state.color = 1
    elif  pattern[1] =< mod_sum_val and mod_sum_val < pattern[2]:
      state.color = 2
    state.step = state.step + 1




class proper:
  def __init__(self,position=100,cycle=90,pattern=[30,30,30],dt = 0.1):
    self.p = position
    self.cycle = cycle
    self.pattern = pattern
    self.dt = dt
class state:
  def __init__( self,color=2,step=0):
    self.color = color
    self.step = step

