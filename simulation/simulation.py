class simulation:
  def __init__(self,timestep,dt,na):

    ##[simulation]
    self.timestep = timestep #number of timestep
    self.dt = dt             #timestep size
    self.na = na

  def set_agent(self,agent=None):
    ##[agent]
    self.agent = agent

  def add_agent(self,agent):
    self.agent.append(agent)


  def set_road(self,road):
    ##[road]
    self.road = road

  
  def agent_state_update(self):
    print('state_update')
    if self.agent == None:
      return
    self.agent_d_update()
    self.agent_v_lead_update()

    self.agent_v_update()
    self.agent_p_update()
    self.agent_v_lead_update()
    self.agent_d_update()
    self.agent_step_update()
    self.agent_d_check()


  def agent_v_update(self):
    for i in range(len(self.agent)):
      self.agent[i].state_v_update()

  def agent_a_update(self):
    for i in range(len(self.agent)):
      self.agent[i].state_a_update()


  def agent_p_update(self): #ここから三つはコピーして渡した方がいいかもしれない、、、
    self.agent = self.road.p_update(self.agent)

  def agent_v_lead_update(self):
    self.agent = self.road.v_lead_update(self.agent)

  def agent_step_update(self):
    for i in range(len(self.agent)):
      self.agent[i].state.step = self.agent[i].state.step + 1

  def agent_d_update(self):
    self.agent = self.road.d_update(self.agent)

  def agent_d_check(self):
    if self.road.shape == 'circle':
      for i in range(self.na):
        if self.agent[i].state.d < 0:
          print("distance is less than 0")
          return 0

    elif self.road.shape == 'line':
      for i in range(self.na-1):
        if self.agent[i].state.d < 0:
          print("distance is less than 0")
          return 0



  def get_agent(self):
    return self.agent


